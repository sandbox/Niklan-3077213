<?php


use Drupal\data_fabric\Extractor\ChainExtractor;

class DataFabric {

  /**
   * The extractor.
   *
   * @var \Drupal\data_fabric\Extractor\ChainExtractor
   */
  protected $extractor;

  public function __construct(array $extractors = [], array $transformers = [], array $loaders = []) {
    $this->extractor = new ChainExtractor($extractors);
  }

  public function extract($data, array $context = []) {
    if (!$this->supportsExtraction($data, $context)) {
      // trow
    }

    return $this->extractor->extract($data, $context);
  }

  protected function supportsExtraction($data, array $context) {

  }

}
