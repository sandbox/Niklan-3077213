<?php

namespace Drupal\data_fabric\Extractor;

class ChainExtractor {

  /**
   * The extractors.
   *
   * @var \Drupal\data_fabric\Extractor\Extractor[]
   */
  protected $extractors = [];

  public function __construct(array $extractors = []) {
    $this->extractors = $extractors;
  }

  public function extract($data, array $context = []) {
    return $this->getExtractor($context)->extract($data, $context);
  }

  protected function getExtractor(array $context): Extractor {
    foreach ($this->extractors as $extractor) {
      if ($extractor->supportsExtraction($context)) {
        return $extractor;
      }
    }

    throw new \RuntimeException('Extractor not found.');
  }

}
